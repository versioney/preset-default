---
title: About versioney
---

# About versioney

> This is a page.
> Pages are declared in the vyfile as:
> ```toml
> [content.pages]
>   index = false
> ```
> which means no index page will be built.

You can find more at [versioney homepage](ujj.space/versioney).
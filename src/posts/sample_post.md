---
title: Example post
date: 2020-02-01
---

# Example post

> This is a post entry.
> posts are declared in the vyfile as:
> ```toml
> [content.posts]
>   index = true 
>   order = "asc" 
> ```
> which means an index page will be built, 
> and posts will be ordered by ascending date.

This is an example post.
---
title: Markdown features
---

# Markdown features

> This is a topic entry.
> topics are declared in the vyfile as:
> ```toml
>    [content.topics]
>        index = true 
>        order = "asc" 
> ```
> which means an index page will be built, 
> and topics will be ordered by ascending date.
You can use markdown features on entries.